public class SimpleWar{
	public static void main(String[] args){
		Deck deck = new Deck();
		deck.shuffle();

		double playerOnePoints = 0;
		double playerTwoPoints = 0;
		
		while(deck.length() > 0){
			Card playerOneCard = deck.drawTopCard();
			Card playerTwoCard = deck.drawTopCard();

			playerOnePoints = playerOneCard.calculateScore();
			playerTwoPoints = playerTwoCard.calculateScore();
			
			if(playerOnePoints > playerTwoPoints){
				System.out.println("Player One has Won the Game");
				System.out.println("Player1 Card: "+ playerOneCard + "		" + playerOneCard.calculateScore());
				System.out.println("Player2 Card: "+ playerTwoCard + "		" + playerTwoCard.calculateScore() + "\n");
				playerOnePoints+=1;
			} else if(playerOnePoints < playerTwoPoints){
				System.out.println("Player Two has Won the Game");
				System.out.println("Player1 Card: "+ playerOneCard + "		" + playerOneCard.calculateScore());
				System.out.println("Player2 Card: "+ playerTwoCard + "		" + playerTwoCard.calculateScore() + "\n");
				playerTwoPoints+=1;
			}
			
			
		}
		System.out.println("\n");
		
		
		if(playerOnePoints > playerTwoPoints){
			System.out.println("CONGRATUALTION TO PLAYER ONE FOR WINNING THE ENTIRE GAME");
		}else if(playerOnePoints < playerTwoPoints){
			System.out.println("CONGRATUALTION TO PLAYER TWO FOR WINNING FOR WINNING THE ENTIRE GAME");
			playerTwoPoints+=1;
		}else{
			System.out.println("BOTH PLAYERS LOST THE GAME");
		}
		
	}
	
	
}