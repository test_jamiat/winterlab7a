import java.util.Random;
public class Deck{
	private Card[] stack;
	private int numberOfCards;
	private Random rng;
	
	public Deck(){
		rng = new Random();
		int i = 0;
		this.numberOfCards = 52;
		this.stack = new Card[52];
		//hearts U+2665
		//spades U+2660
		//clubs U+2663
		//diamonds U+2666
		
		String[] suits = new String[]{"Hearts", "Spades", "Clubs", "Diamonds"};
		int[] ranks = new int[]{1,2,3,4,5,6,7,8,9,10,11,12,13};
		
		for(int s = 0; s < suits.length; s++){
			for(int r = 0; r < ranks.length; r++){
				stack[i] = new Card(suits[s], ranks[r]);
				i++;
			}
		}
	}
	
	public int length(){
		return this.numberOfCards;
	}
	
	public Card drawTopCard(){
		this.numberOfCards--;
		return stack[this.numberOfCards];
	}
	
	public String toString(){
		String allCards = "";
		for(int i = 0; i < this.numberOfCards; i++){
			allCards += this.stack[i] + "\n";
		}
		return allCards;
	}
	
	public void shuffle(){
		for(int i = 0; i < this.numberOfCards; i++){
			int randomCard = rng.nextInt(i, this.numberOfCards);
			Card chosenCard = this.stack[i];
			this.stack[i] = this.stack[randomCard];
			this.stack[randomCard] = chosenCard;
		}
	}
}