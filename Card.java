public class Card{
	private String suit;
	private int value;
	
	public Card(String suit, int value){
		this.suit = suit;
		this.value = value;
	}
	
	public String getSuit(){
		return this.suit;
	}
	
	public void setSuit(String suit){
		this.suit = suit;
	}
	
	public int getValue(){
		return this.value;
	}
	public void setValue(int value){
		this.value = value;
	}
	
	public double calculateScore(){
		double score = value;
		// double decimalValue = value + 0.0;
		if(suit.equals("Hearts")){
			score +=0.4;
		}
		if(suit.equals("Spades")){
			score += 0.3;
		}
		if(suit.equals("Diamonds")){
			score += 0.2;
		}
		if(suit.equals("Clubs")){
			score += 0.1;
		}
		return score;
	}
	
	public String toString(){
		String valueToString = String.valueOf(value);
		
		if(value == 1){valueToString = "Ace";}
		if(value == 11){valueToString = "Jack";}
		if(value == 12){valueToString = "Queen";}
		if(value == 13){valueToString = "King";}
		
		return valueToString  + " of " + suit;
	}
	
	
}